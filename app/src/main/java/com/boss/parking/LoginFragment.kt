package com.boss.parking

import android.os.Bundle
import android.view.*
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import androidx.navigation.ui.NavigationUI

class LoginFragment: Fragment() {
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_login,container,false)
        val loginbtn: Button = view.findViewById(R.id.loginBtn)
        var username: EditText = view.findViewById(R.id.username_txt)
        var password: EditText = view.findViewById(R.id.password_txt)
        loginbtn.setOnClickListener {
            val user = username.text.toString()
            val pwd = password.text.toString()
            if(user.equals("admin") && pwd.equals("12345678")){
                view.findNavController().navigate(R.id.action_loginFragment_to_parkingFragment)
            }else{
                Toast.makeText(
                    this.context, "Username or Password Invalid",
                    Toast.LENGTH_LONG
                ).show();
            }
            username.text.clear()
            password.text.clear()
        }
        setHasOptionsMenu(true)
        return view
    }override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return NavigationUI.onNavDestinationSelected(item!!,
            view!!.findNavController())
                || super.onOptionsItemSelected(item)
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater?.inflate(R.menu.options_menu, menu)
    }
}